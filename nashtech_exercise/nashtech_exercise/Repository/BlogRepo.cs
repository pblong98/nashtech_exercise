﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using nashtech_exercise.Data;
using nashtech_exercise.Entities;
using nashtech_exercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace nashtech_exercise.Repository
{
    public class BlogRepo
    {
        private readonly ApplicationDbContext _context;

        public BlogRepo(ApplicationDbContext context)
        {
            _context = context;
        }

        public BlogModel Detail (long blogId, int commentPage, int pageSize, ref int commentTotal)
        {
            var result = _context.Blog
                .Where(b => b.BlogId == blogId)
                .Include(b=>b.AuthorId)
                .FirstOrDefault();

            int _commentTotal = _context.Comment
                .Where(c => c.Blog == result)
                .Count();
            commentTotal = _commentTotal;

            if (commentPage <= 0)
            {
                commentPage = 1;
            }

            if (commentPage > _commentTotal)
            {
                commentPage = _commentTotal;
            }

            if (pageSize <= 5)
            {
                pageSize = 5;
            }

            int skip = 0, take = pageSize;

            take = pageSize;
            skip = pageSize * commentPage - pageSize;

            List<Comment> _comments = null;
            if(_commentTotal > 0)
            {
                _comments = _context.Comment
                .Where(c => c.Blog == result)
                .OrderByDescending(c => c.CreationDate)
                .Skip(skip)
                .Take(take)
                .Include(c => c.AuthorId)
                .ToList();
            }

            string authorName = "", authorId = "";
            if (result.AuthorId != null)
            {
                authorName = result.AuthorId.UserName;
                authorId = result.AuthorId.Id;
            }

            List<CommentModel> comments = new List<CommentModel>();
            if (_comments != null)
            {
                foreach(var i in _comments)
                {
                    string commentAuthorName = "", commentAuthorId = "";
                    if(i.AuthorId != null)
                    {
                        commentAuthorName = i.AuthorId.UserName;
                        commentAuthorId = i.AuthorId.Id;
                    }
                    comments.Add(new CommentModel { 
                        AuthorName = commentAuthorName,
                        CommentId = i.CommentId,
                        Content = i.Content,
                        CreationDate = i.CreationDate,
                        AuthorId = commentAuthorId
                    });
                }
            }

            return new BlogModel { 
                Description = result.Description,
                Name = result.Name,
                CreationDate = result.CreationDate,
                ImgUrl = result.ImgUrl,
                AuthorName = authorName,
                BlogId = result.BlogId,
                Comments = comments,
                AuthorId = authorId
            };
        }

        public bool Create(BlogModel blog, string authorId)
        {

            Blog newBlog = new Blog
            {
                ImgUrl = blog.ImgUrl,
                CreationDate = DateTime.Now,
                AuthorId = _context.Users.Find(authorId),
                Description = blog.Description,
                Name = blog.Name
            };

            _context.Blog.Add(newBlog);
            _context.SaveChanges();
            return _context.Blog.Find(blog.BlogId) != null;
        }

        public PagingModel GetPagingModel(int currentPage, int pageSize)
        {
            int totalPage = _context.Blog.Count() / (pageSize + 1) + 1;

            return new PagingModel
            {
                CurrentPage = currentPage,
                TotalPage = totalPage
            };
        }

        public bool Delete(long blogId, string currentUserId)
        {
            var blog = _context.Blog
                .Where(b=>b.BlogId == blogId)
                .Include(b=>b.Comments)
                .Include(b => b.AuthorId)
                .FirstOrDefault();
            if(blog != null)
            {
                if(blog.AuthorId.Id == currentUserId)
                {
                    _context.Comment.RemoveRange(blog.Comments);
                    _context.Blog.Remove(blog);
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public void CreateComment(long blogId, string comment, string authorId)
        {
            try
            {
                IdentityUser currentUser = _context.Users.Find(authorId);
                Blog currentBlog = _context.Blog.Find(blogId);
                _context.Comment.Add(new Comment
                {
                    AuthorId = currentUser,
                    Blog = currentBlog,
                    Content = comment,
                    CreationDate = DateTime.Now
                });
                _context.SaveChanges();
            }
            catch { }
        }

        public bool DeleteComment(long commentId, string currentUserId)
        {
            var comment = _context.Comment
                .Where(c => c.CommentId == commentId)
                .Include(c => c.AuthorId)
                .Include(c => c.Blog)
                    .ThenInclude(a => a.AuthorId)
                .FirstOrDefault();

            if (comment != null)
            {
                if (comment.AuthorId.Id == currentUserId || comment.Blog.AuthorId.Id == currentUserId)
                {
                    _context.Comment.Remove(comment);
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }
    }
}
