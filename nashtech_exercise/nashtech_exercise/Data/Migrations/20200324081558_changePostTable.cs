﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace nashtech_exercise.Data.Migrations
{
    public partial class changePostTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Blog");

            migrationBuilder.AddColumn<string>(
                name: "AuthorIdId",
                table: "Blog",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Blog_AuthorIdId",
                table: "Blog",
                column: "AuthorIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blog_AspNetUsers_AuthorIdId",
                table: "Blog",
                column: "AuthorIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blog_AspNetUsers_AuthorIdId",
                table: "Blog");

            migrationBuilder.DropIndex(
                name: "IX_Blog_AuthorIdId",
                table: "Blog");

            migrationBuilder.DropColumn(
                name: "AuthorIdId",
                table: "Blog");

            migrationBuilder.AddColumn<string>(
                name: "AuthorId",
                table: "Blog",
                type: "nvarchar(450)",
                maxLength: 450,
                nullable: true);
        }
    }
}
