﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace nashtech_exercise.Data.Migrations
{
    public partial class addImgBlog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImgUrl",
                table: "Blog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImgUrl",
                table: "Blog");
        }
    }
}
