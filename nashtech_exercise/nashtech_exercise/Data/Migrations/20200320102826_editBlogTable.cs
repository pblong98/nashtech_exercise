﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace nashtech_exercise.Data.Migrations
{
    public partial class editBlogTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullDescription",
                table: "Blog");

            migrationBuilder.DropColumn(
                name: "ShortDescription",
                table: "Blog");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "Blog",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Blog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "Blog");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Blog");

            migrationBuilder.AddColumn<string>(
                name: "FullDescription",
                table: "Blog",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortDescription",
                table: "Blog",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }
    }
}
