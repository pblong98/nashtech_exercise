﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using nashtech_exercise.Data;
using nashtech_exercise.Entities;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;
using nashtech_exercise.Repository.Home;

namespace nashtech_exercise.Controllers
{
    public class BlogController : Controller
    {
        private readonly ILogger<BlogController> _logger;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly BlogRepo blogRepo;

        public BlogController(ILogger<BlogController> logger, ApplicationDbContext _context, UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            blogRepo = new BlogRepo(_context);
            _userManager = userManager;
        }

        public IActionResult Detail(long blogId, int? commentPage)
        {
            if (commentPage == null || commentPage <= 0)
            {
                commentPage = 1;
            }
            int totalPage = 1;
            var blog = blogRepo.Detail(blogId, commentPage.Value, 5 ,ref totalPage);
            ViewBag.CommentPage = commentPage;
            ViewBag.TotalPage = totalPage / (5 + 1) + 1;
            return View(blog);
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        public IActionResult Delete(long blogId)
        {
            blogRepo.Delete(blogId, _userManager.GetUserId(User));
            return Redirect("/");
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public IActionResult Create( BlogModel blog, List<IFormFile> files)
        {
            //UserManager
            if (ModelState.IsValid)
            {
                string imgUrl = "";
                if (files.Count > 0)
                {
                    imgUrl = UploadImg(files[0]);
                }

                blog.ImgUrl = imgUrl;
                bool isOk = blogRepo.Create(blog, _userManager.GetUserId(User));

                if (isOk)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    //return RedirectToAction(error);
                    //return null;
                }
                return Redirect("/");
            }
            else
            {
                return View(blog);
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateComment(long blogId, string comment)
        {
            blogRepo.CreateComment(blogId, comment, _userManager.GetUserId(User));
            return RedirectToAction("Detail", "Blog", new { blogId = blogId });
        }

        [Authorize]
        public IActionResult DeleteComment(long blogId, long commentId)
        {
            blogRepo.DeleteComment(commentId, _userManager.GetUserId(User));
            return RedirectToAction("Detail", "Blog", new { blogId = blogId });
        }

        private string UploadImg(IFormFile file)
        {
            var guiId = Guid.NewGuid().ToString();

            string filePath = "wwwroot/Upload/" + guiId + file.FileName;
            filePath = filePath.Remove(filePath.LastIndexOf("."));
            string fileThumbPath = "wwwroot/Upload/thumb_" + guiId + file.FileName;
            fileThumbPath = fileThumbPath.Remove(fileThumbPath.LastIndexOf("."));

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            using (var stream = new FileStream(fileThumbPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            Image_resize(filePath, filePath+".jpg", 700);
            Image_resize(fileThumbPath, fileThumbPath+".jpg", 220);

            System.IO.File.Delete(filePath);
            System.IO.File.Delete(fileThumbPath);

            return filePath.Replace("wwwroot","")+".jpg";
        }

        private void Image_resize(string input_Image_Path, string output_Image_Path, int new_Width)
        {
            const long quality = 100L;
            Bitmap source_Bitmap = new Bitmap(input_Image_Path);
            double dblWidth_origial = source_Bitmap.Width;
            double dblHeigth_origial = source_Bitmap.Height;
            double relation_heigth_width = dblHeigth_origial / dblWidth_origial;
            int new_Height = (int)(new_Width * relation_heigth_width);

            //< create Empty Drawarea >
            var new_DrawArea = new Bitmap(new_Width, new_Height);

            //</ create Empty Drawarea >
            using (var graphic_of_DrawArea = Graphics.FromImage(new_DrawArea))
            {
                //< setup >
                graphic_of_DrawArea.CompositingQuality = CompositingQuality.HighSpeed;
                graphic_of_DrawArea.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic_of_DrawArea.CompositingMode = CompositingMode.SourceCopy;
                //</ setup >
                //< draw into placeholder >
                //*imports the image into the drawarea
                graphic_of_DrawArea.DrawImage(source_Bitmap, 0, 0, new_Width, new_Height);
                //</ draw into placeholder >
                //--< Output as .Jpg >--
                using (var output = System.IO.File.Open(output_Image_Path, FileMode.Create))
                {
                    //< setup jpg >
                    var qualityParamId = Encoder.Quality;
                    var encoderParameters = new EncoderParameters(1);
                    encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                    //</ setup jpg >
                    //< save Bitmap as Jpg >
                    var codec = ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == ImageFormat.Jpeg.Guid);
                    new_DrawArea.Save(output, codec, encoderParameters);
                    //resized_Bitmap.Dispose();
                    output.Close();
                    //</ save Bitmap as Jpg >
                }
                //--</ Output as .Jpg >--
                graphic_of_DrawArea.Dispose();
            }
            source_Bitmap.Dispose();
        }
    }
}