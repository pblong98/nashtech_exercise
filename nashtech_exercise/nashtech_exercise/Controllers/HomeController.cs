﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using nashtech_exercise.Data;
using nashtech_exercise.Models;
using nashtech_exercise.Repository;
using nashtech_exercise.Repository.Home;

namespace nashtech_exercise.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly HomeRepo homeRepo;
        private readonly BlogRepo blogRepo;
        public HomeController(ILogger<HomeController> logger, ApplicationDbContext _context)
        {
            _logger = logger;
            homeRepo = new HomeRepo(_context);
            blogRepo = new BlogRepo(_context);
        }

        public IActionResult Index(int page = 1)
        {
            if(page <= 0)
            {
                page = 1;
            }
            List<BlogModel> blogList = homeRepo.GetList(page, 8);
            PagingModel pagingModel = blogRepo.GetPagingModel(page, 8);
            ViewBag.PagingModel = pagingModel;
            return View(blogList);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
