﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_exercise.Models
{
    public class CommentModel
    {
        public long CommentId { get; set; }
        public string AuthorName { get; set; }
        [MaxLength(1000)]
        public string Content { get; set; }
        public DateTime CreationDate { get; set; }
        public string AuthorId {get;set; }
    }
}
