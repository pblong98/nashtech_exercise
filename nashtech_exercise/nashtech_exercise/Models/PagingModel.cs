﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_exercise.Models
{
    public class PagingModel
    {
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }
        public long BlogId { get; set; }
    }
}
