﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace nashtech_exercise.Models
{
    public class BlogModel
    {
        public long BlogId { get; set; }  //BlogId, Name, Description, AuthorName, CreationDate

        [Required]
        [MaxLength(100)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        public string AuthorName { get; set; }
        public string AuthorId { get; set; }

        [Required(ErrorMessage = "Creation Date field is requied")]
        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }
        public String ImgUrl { get; set; }
        public List<CommentModel> Comments {get; set; }

        public int totalComment { get; set; }

    }
}
